package com.mdagis.myapp.domain.model.channel;

/**
 *
 * @author agis
 */
public interface ITelephoneRepository {
    
    Telephone findById(Integer id);
    
    Telephone addToPerson(Integer personId, String telephone);
    
}
