
package com.mdagis.myapp.domain.model.person;

/**
 *
 * @author agis
 */
public interface IPersonRepository {
    
    Person findById(Integer id);
    
    Person createPerson(String firstName, String lastName);
    
    void echo();
    
}
