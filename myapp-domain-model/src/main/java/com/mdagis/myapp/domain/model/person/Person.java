package com.mdagis.myapp.domain.model.person;

import com.mdagis.myapp.domain.model.channel.Telephone;
import java.util.ArrayList;
import java.util.List;
import javax.validation.constraints.Size;

/**
 *
 * @author agis
 */
public class Person {
    
    private Integer Id;
    @Size(max = 80)
    private String lastName;
    @Size(max = 80)
    private String firstName;
    private final List<Telephone> telephoneList = new ArrayList<>();

    private Person() {
    }

    public Person(String lastName, String firstName) {
        this.lastName = lastName;
        this.firstName = firstName;
    }

    public Integer getId() {
        return Id;
    }

    public String getLastName() {
        return lastName;
    }

    public String getFirstName() {
        return firstName;
    }

    public List<Telephone> getTelephoneList() {
        return telephoneList;
    }
    
}
