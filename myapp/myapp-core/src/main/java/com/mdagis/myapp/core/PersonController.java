
package com.mdagis.myapp.core;

/**
 *
 * @author agis
 */
import com.mdagis.myapp.domain.model.person.IPersonRepository;
import com.mdagis.myapp.domain.model.qualifiers.JPARepo;
import javax.ejb.Stateless;
import javax.inject.Inject;

/**
 *
 * @author agis
 */
@Stateless
public class PersonController {
    
    @Inject @JPARepo private IPersonRepository repo;
    
    public void doSomething() {
        repo.echo();
        repo.createPerson("agis", "kefallinos");
    }
    
}
