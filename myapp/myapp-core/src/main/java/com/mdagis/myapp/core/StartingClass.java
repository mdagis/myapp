
package com.mdagis.myapp.core;

import javax.annotation.PostConstruct;
import javax.ejb.Singleton;
import javax.ejb.Startup;
import javax.inject.Inject;

/**
 *
 * @author agis
 */
@Startup
@Singleton
public class StartingClass {
    
    @Inject private PersonController pc;
    
    
    @PostConstruct
    public void starting() {
        pc.doSomething();
    }
    
}
