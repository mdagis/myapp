package com.mdagis.myapp.persistence.jpa.channel;


import com.mdagis.myapp.persistence.jpa.person.PersonEntity;
import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.validation.constraints.Size;

/**
 *
 * @author agis
 */
@Entity
@Table(name = "telephone")
public class TelephoneEntity implements Serializable {
    
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "id")
    private Integer Id;
    @JoinColumn(name = "PersonId", referencedColumnName = "id", nullable = false)
    @ManyToOne(optional = false)
    private PersonEntity person;
    @Basic(optional = false)
    @Size(max = 20)
    @Column(name = "TelephoneNumber", nullable = false, length = 20)
    private String telephoneNumber;

    public TelephoneEntity() {
    }

    public Integer getId() {
        return Id;
    }

    public void setId(Integer Id) {
        this.Id = Id;
    }

    public PersonEntity getPerson() {
        return person;
    }

    public void setPerson(PersonEntity person) {
        this.person = person;
    }

    public String getTelephoneNumber() {
        return telephoneNumber;
    }

    public void setTelephoneNumber(String telephoneNumber) {
        this.telephoneNumber = telephoneNumber;
    }
    
    

}
