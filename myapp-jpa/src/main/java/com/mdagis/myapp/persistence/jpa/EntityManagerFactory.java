package com.mdagis.myapp.persistence.jpa;

import javax.ejb.Stateless;
import javax.enterprise.inject.Produces;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

/**
 *
 * @author agis
 */
@Stateless
public class EntityManagerFactory {

    @PersistenceContext(unitName = "mypu")
    private EntityManager entityManager;

    @Produces
    public EntityManager produceEntityManager() {
        return entityManager;
    }
}
