
package com.mdagis.myapp.persistence.jpa.channel;

import com.mdagis.myapp.domain.model.channel.ITelephoneRepository;
import com.mdagis.myapp.domain.model.channel.Telephone;
import com.mdagis.myapp.domain.model.qualifiers.JPARepo;
import javax.ejb.Stateless;
import javax.inject.Inject;
import javax.persistence.EntityManager;

/**
 *
 * @author agis
 */
@Stateless
@JPARepo
public class TelephoneJPARepository implements ITelephoneRepository{
    
    @Inject
    private EntityManager entityManager;

    @Override
    public Telephone findById(Integer id) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public Telephone addToPerson(Integer personId, String telephone) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }
    
}
