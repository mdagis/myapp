
package com.mdagis.myapp.persistence.jpa.person;

import com.mdagis.myapp.domain.model.person.IPersonRepository;
import com.mdagis.myapp.domain.model.person.Person;
import com.mdagis.myapp.domain.model.qualifiers.JPARepo;
import javax.ejb.Stateless;
import javax.inject.Inject;
import javax.persistence.EntityManager;

/**
 *
 * @author agis
 */
@Stateless
@JPARepo
public class PersonJPARepository implements IPersonRepository{
    
    @Inject
    private EntityManager entityManager;

    @Override
    public Person findById(Integer id) {
        PersonEntity person = entityManager.find(PersonEntity.class, id);
        return new Person(person.getLastName(), person.getFirstName());
    }


    @Override
    public Person createPerson(String firstName, String lastName) {
        
        
        
        PersonEntity person = new PersonEntity();
        person.setFirstName(firstName);
        person.setLastName(lastName);
        entityManager.persist(person);
        return new Person(person.getLastName(), person.getFirstName());
    }

    @Override
    public void echo() {
        System.out.println(" echo from " + this.getClass().getName());
    }
    
}
